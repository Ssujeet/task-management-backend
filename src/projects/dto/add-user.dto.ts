import { IsArray, IsNotEmpty } from "class-validator";
import { User } from "src/user/entities/user.entity";

export class AddUserDTo{


  @IsArray()
  @IsNotEmpty()
  
  users:User[]
}
