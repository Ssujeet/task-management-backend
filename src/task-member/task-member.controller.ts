import { Body, ClassSerializerInterceptor,Controller, Delete,Get, Param, Patch, Post, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RESPONSESTATUS } from 'src/common/enums/enums';
import { returnResponse } from 'src/common/utils/utils.functions';
import { BaseApiResponse } from 'src/shared/dtos/base-api-response.dto';
import { ReqContext } from 'src/shared/request-context/req-context.decorator';
import { RequestContext } from 'src/shared/request-context/request-context.dto';
import { OutputTaskDto } from 'src/tasks/dto/output-task.dto';

import { CreateTaskMemberDto } from './dto/create-task-member.dto';
import { UpdateTaskMemberDto } from './dto/update-task-member.dto';
import { TaskMemberService } from './task-member.service';

@Controller('task-member')
export class TaskMemberController {
  constructor(private readonly taskMemberService: TaskMemberService) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  async create(@ReqContext() ctx: RequestContext, @Body() createTaskMemberDto: CreateTaskMemberDto):Promise<BaseApiResponse<OutputTaskDto>> {
    
    const taskMember = await this.taskMemberService.create(ctx,createTaskMemberDto);
    return returnResponse(taskMember, "SuccessFully Added", RESPONSESTATUS.SUCCESS, {})
  }



  @Get()
  findAll() {
    return this.taskMemberService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.taskMemberService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTaskMemberDto: UpdateTaskMemberDto) {
    return this.taskMemberService.update(+id, updateTaskMemberDto);
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  async removeUserFromTasks(@Param('id') id: string): Promise<BaseApiResponse<OutputTaskDto>> {
    const taskMember = await this.taskMemberService.removeUserFromTask(+id);
    return returnResponse(taskMember, "SuccessFully Removed", RESPONSESTATUS.SUCCESS, {})
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.taskMemberService.remove(+id);
  }
}
