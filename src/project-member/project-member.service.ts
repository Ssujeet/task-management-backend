import { Injectable, UnauthorizedException } from '@nestjs/common';
import { plainToClass, plainToInstance } from 'class-transformer';
import { PROJECTMEMBERROLE } from 'src/common/enums/enums';
import { ProjectsService } from 'src/projects/projects.service';
import { BaseApiResponse } from 'src/shared/dtos/base-api-response.dto';
import { RequestContext } from 'src/shared/request-context/request-context.dto';
import { User } from 'src/user/entities/user.entity';
import { UserService } from 'src/user/services/user.service';

import { CreateProjectMemberDto } from './dto/create-project-member.dto';
import { ProjectMemberbyProjectId } from './dto/get-project-members-by-projectId.dto';
import { ProjectListsDto } from './dto/projects-lists.dto';
import { UpdateProjectMemberDto } from './dto/update-project-member.dto';
import { ProjectMember } from './entities/project-member.entity';
import { ProjectMemberRepository } from './repositories/project-member.respositories';

@Injectable()
export class ProjectMemberService {
  constructor(
    private projectMemberRespository: ProjectMemberRepository,
    private userService: UserService,
    private projectService: ProjectsService,
    
  ) {
    // this.logger.setContext(ProjectsService.name);
  }


  async update(ctx: RequestContext,updateProjectMemberDto: UpdateProjectMemberDto): Promise<UpdateProjectMemberDto| string> {
  const {projectId} = updateProjectMemberDto
    const userDetailsInProject = await this.getCurrentUserDetailsInProject(ctx, projectId);

    if (!userDetailsInProject || userDetailsInProject.role === PROJECTMEMBERROLE.MEMBER) {
      throw new UnauthorizedException()
    }

    const updatedProjectMember: ProjectMember = { ...updateProjectMemberDto, ...plainToClass(ProjectMember, updateProjectMemberDto) }

    
    
    await this.projectMemberRespository.save(updatedProjectMember)
    return plainToClass(UpdateProjectMemberDto, updatedProjectMember, {
     
    })
  }


  remove(id: number) {
    return `This action removes a #${id} projectMember`;
  }



  async getProjectMemberListsByProjectId(id: number): Promise<ProjectMemberbyProjectId[]>{
    const projectMember = await this.projectMemberRespository.getProjectMembersByProjectId(id)
    return plainToClass(ProjectMemberbyProjectId, projectMember)
  }
  

  async getCurrentUserDetailsInProject(ctx: RequestContext, projectId: number): Promise<ProjectMemberbyProjectId>{
    

    const userDetail = await this.projectMemberRespository.getCurrentCurrentDetailsInProjects(ctx, projectId)
    
    
    return plainToClass(ProjectMemberbyProjectId, userDetail, {excludeExtraneousValues:true})
  }


  async getCurrentUserProjectLists(ctx: RequestContext): Promise<ProjectListsDto[]> {
    const projectLists = await this.projectMemberRespository.getProjectsListForCurrentUsers(ctx)
    return plainToClass(ProjectListsDto, projectLists)
  }

  async addProjectMemberInProjectbyProjectIdAndUserId(ctx: RequestContext,createMember:CreateProjectMemberDto): Promise<ProjectMemberbyProjectId| string>{

    const { userId, projectId, role } = createMember
    const userDetailsInProject = await this.getCurrentUserDetailsInProject(ctx, projectId);

    if (!userDetailsInProject || userDetailsInProject.role === PROJECTMEMBERROLE.MEMBER) {
      throw new UnauthorizedException();
    }
    const project = await this.projectService.findOne(projectId)
    const user = await  this.userService.getUserById(ctx, +userId)
    
    const newProjectMember = new ProjectMember()
    newProjectMember.role = role;
    newProjectMember.user = plainToInstance(User, user);
    newProjectMember.project = project
    this.projectMemberRespository.save(newProjectMember)

    return plainToClass(ProjectMemberbyProjectId, newProjectMember, { excludeExtraneousValues: true })
  }

  
}
