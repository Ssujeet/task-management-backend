import { Test, TestingModule } from '@nestjs/testing';

import { ProjectFilesService } from './project-files.service';

describe('ProjectFilesService', () => {
  let service: ProjectFilesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProjectFilesService],
    }).compile();

    service = module.get<ProjectFilesService>(ProjectFilesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
