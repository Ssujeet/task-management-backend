import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

import { CreateTaskStatusDto } from './create-task-status.dto';

export class UpdateTaskStatusDto extends CreateTaskStatusDto {

  @IsNotEmpty()
  @ApiProperty()
  id: number;


}
