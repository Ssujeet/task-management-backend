import { NotFoundException } from "@nestjs/common";
import { EntityRepository, Repository } from "typeorm";

import { Project } from "../entities/project.entity";

@EntityRepository(Project)
export class ProjectRepository extends Repository<Project>{
  async getById(id: number): Promise<Project>{
    const project = await this.findOne(id);
    if (!project) {
      throw new NotFoundException();
    }
    return project
  }

}
