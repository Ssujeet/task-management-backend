import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { BaseApiResponse } from 'src/shared/dtos/base-api-response.dto';
import { ReqContext } from 'src/shared/request-context/req-context.decorator';
import { RequestContext } from 'src/shared/request-context/request-context.dto';

import { CreateProjectDto } from './dto/create-project.dto';
import { ProjectOutputDto } from './dto/project-output.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { ProjectsService } from './projects.service';

@Controller('projects')
export class ProjectsController {
  constructor(private readonly projectsService: ProjectsService) {}

  @Post()
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async create(@ReqContext() ctx: RequestContext, @Body() createProjectDto: CreateProjectDto): Promise<BaseApiResponse<ProjectOutputDto>> {

    const project = await this.projectsService.create(ctx, createProjectDto);
    return {data:project, meta:{}}
  }



  @Get(':id')
  async findOne(@Param('id') id: string): Promise<BaseApiResponse<ProjectOutputDto>> {
    const projectService = await this.projectsService.findOne(+id);
    return {data:projectService, meta:{}}
  }

  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateProjectDto: UpdateProjectDto): Promise<BaseApiResponse<ProjectOutputDto>> {
    const updatedProject = await this.projectsService.update(+id, updateProjectDto);
    return { data: updatedProject, meta: {} }
  }


}
