import { Body, ClassSerializerInterceptor,Controller, Delete,Get, Param, Patch, Post, Put,UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { BaseApiResponse } from 'src/shared/dtos/base-api-response.dto';
import { ReqContext } from 'src/shared/request-context/req-context.decorator';
import { RequestContext } from 'src/shared/request-context/request-context.dto';

import { CreateProjectMemberDto } from './dto/create-project-member.dto';
import { ProjectMemberbyProjectId } from './dto/get-project-members-by-projectId.dto';
import { ProjectListsDto } from './dto/projects-lists.dto';
import { UpdateProjectMemberDto } from './dto/update-project-member.dto';
import { ProjectMemberService } from './project-member.service';

@Controller('project-member')
export class ProjectMemberController {
  constructor(private readonly projectMemberService: ProjectMemberService) {}
  
  @Get('lists/:id')
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async findOne(@Param('id') id: string): Promise<BaseApiResponse<ProjectMemberbyProjectId[]>> {
    const projectMember = await this.projectMemberService.getProjectMemberListsByProjectId(+id);
    return {data:projectMember, meta:{} }
  }
  @Get('projects-lists/')
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async getCurrentUserProjectLists(@ReqContext() ctx: RequestContext): Promise<BaseApiResponse<ProjectListsDto[]>> {
    const projectMember = await this.projectMemberService.getCurrentUserProjectLists(ctx);
    return { data: projectMember, meta: {} }
  }


  @Get('currentUser/:id')
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async findUserDetailsInProject(@ReqContext() ctx: RequestContext, @Param('id') id: string): Promise<BaseApiResponse<ProjectMemberbyProjectId>> {
    const projectMemberDetails = await this.projectMemberService.getCurrentUserDetailsInProject(ctx, +id)
   
    return { data: projectMemberDetails || null  ,meta:{}};
  }

  @Post('addUser')
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async addMemberInProject(@ReqContext() ctx: RequestContext, @Body() createProjectMember: CreateProjectMemberDto): Promise<BaseApiResponse<ProjectMemberbyProjectId | string | null>> {
    const projectMemberDetails = await this.projectMemberService.addProjectMemberInProjectbyProjectIdAndUserId(ctx, createProjectMember)
    return { data: projectMemberDetails || null, meta: {} };
  }



  @Put()
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async update(@ReqContext() ctx: RequestContext, @Body() updateProjectMemberDto: UpdateProjectMemberDto):Promise<BaseApiResponse<UpdateProjectMemberDto|string>> {
    const updatedProjectMember = await this.projectMemberService.update(ctx, updateProjectMemberDto);
    return { data: updatedProjectMember, meta:{}}
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.projectMemberService.remove(+id);
  }
}
