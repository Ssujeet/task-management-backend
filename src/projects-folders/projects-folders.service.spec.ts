import { Test, TestingModule } from '@nestjs/testing';

import { ProjectsFoldersService } from './projects-folders.service';

describe('ProjectsFoldersService', () => {
  let service: ProjectsFoldersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProjectsFoldersService],
    }).compile();

    service = module.get<ProjectsFoldersService>(ProjectsFoldersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
