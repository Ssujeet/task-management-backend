import { Injectable } from '@nestjs/common';

import { CreateProjectFileDto } from './dto/create-project-file.dto';
import { UpdateProjectFileDto } from './dto/update-project-file.dto';

@Injectable()
export class ProjectFilesService {
  create(createProjectFileDto: CreateProjectFileDto) {
    return 'This action adds a new projectFile';
  }

  findAll() {
    return `This action returns all projectFiles`;
  }

  findOne(id: number) {
    return `This action returns a #${id} projectFile`;
  }

  update(id: number, updateProjectFileDto: UpdateProjectFileDto) {
    return `This action updates a #${id} projectFile`;
  }

  remove(id: number) {
    return `This action removes a #${id} projectFile`;
  }
}
