import { AbstractEntity } from "src/common/entity/asbstract-entity";
import { PROJECTMEMBERROLE, USERSTATUSINPROJECT } from "src/common/enums/enums";
import { Project } from "src/projects/entities/project.entity";
import { User } from "src/user/entities/user.entity";
import { Column,Entity, ManyToOne } from "typeorm";


@Entity()
export class ProjectMember extends AbstractEntity{
  @ManyToOne(() => Project, project => project.projectMember)
  project: Project;

  @ManyToOne(() => User, user => user.projectMember)
  user: User;

  @Column({
    type: 'enum',
    enum: PROJECTMEMBERROLE,
    default: PROJECTMEMBERROLE.MEMBER})
  role: PROJECTMEMBERROLE

  @Column({
    type: 'enum',
    enum: USERSTATUSINPROJECT,
    default: USERSTATUSINPROJECT.ACTIVE
  })
  userStatus: USERSTATUSINPROJECT


}
