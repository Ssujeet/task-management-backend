import { Expose } from "class-transformer";
import { TASKSPRIORITY } from "src/common/enums/enums";
import { TaskMember } from "src/task-member/entities/task-member.entity";
import { TaskStatus } from "src/task-status/entities/task-status.entity";
import { User } from "src/user/entities/user.entity";

export class OutputTaskDto{
  @Expose()
  title: string;

  @Expose()
  priority: TASKSPRIORITY;

  @Expose()
  taskStatus: TaskStatus;

  @Expose()
  descriptions: string;

  @Expose()
  taskMember: TaskMember[]

  @Expose()
  createdUser: User;

}
