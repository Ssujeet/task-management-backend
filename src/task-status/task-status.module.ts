import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectMemberRepository } from 'src/project-member/repositories/project-member.respositories';
import { ProjectsModule } from 'src/projects/projects.module';
import { SharedModule } from 'src/shared/shared.module';
import { UserModule } from 'src/user/user.module';

import { TaskStatusRepository } from './repositories/taskStatus.repositories';
import { TaskStatusController } from './task-status.controller';
import { TaskStatusService } from './task-status.service';

@Module({
  imports: [TypeOrmModule.forFeature([TaskStatusRepository,ProjectMemberRepository]), UserModule, SharedModule,ProjectsModule],
  controllers: [TaskStatusController],
  providers: [TaskStatusService],
  exports: [TaskStatusService]
})
export class TaskStatusModule {}
