import { Test, TestingModule } from '@nestjs/testing';

import { ProjectFilesController } from './project-files.controller';
import { ProjectFilesService } from './project-files.service';

describe('ProjectFilesController', () => {
  let controller: ProjectFilesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProjectFilesController],
      providers: [ProjectFilesService],
    }).compile();

    controller = module.get<ProjectFilesController>(ProjectFilesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
