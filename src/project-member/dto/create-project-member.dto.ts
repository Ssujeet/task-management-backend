import { ApiProperty } from "@nestjs/swagger";
import { IsEnum,IsNotEmpty, IsNumber } from "class-validator";
import { PROJECTMEMBERROLE } from "src/common/enums/enums";

export class CreateProjectMemberDto {

  @IsNotEmpty()
  @ApiProperty()
  @IsNumber()
  userId: number;

  @IsNotEmpty()
  @ApiProperty()
  @IsNumber()
  projectId: number;
  
  @IsNotEmpty()
  @ApiProperty()
  @IsEnum(PROJECTMEMBERROLE)
  role:PROJECTMEMBERROLE

}
