import { ApiProperty } from "@nestjs/swagger";
import { IsArray,IsEnum,IsNotEmpty, IsNumber,IsOptional,IsString } from "class-validator";
import { TASKSPRIORITY } from "src/common/enums/enums";
import { TaskStatus } from "src/task-status/entities/task-status.entity";


export class CreateTaskDto {

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  title: string;


  @ApiProperty()
  @IsEnum(TASKSPRIORITY)
  @IsNotEmpty()
  priority: TASKSPRIORITY;


  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  taskStatusId: number;



  @ApiProperty()
  @IsOptional()
  descriptions: string;


  @ApiProperty()
  @IsArray()
  taskMemberId:number[]




}
