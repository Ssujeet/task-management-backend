import { Test, TestingModule } from '@nestjs/testing';

import { ProjectsFoldersController } from './projects-folders.controller';
import { ProjectsFoldersService } from './projects-folders.service';

describe('ProjectsFoldersController', () => {
  let controller: ProjectsFoldersController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProjectsFoldersController],
      providers: [ProjectsFoldersService],
    }).compile();

    controller = module.get<ProjectsFoldersController>(
      ProjectsFoldersController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
