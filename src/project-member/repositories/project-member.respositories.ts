
import { plainToClass } from "class-transformer";
import { PROJECTSTATUS, USERSTATUSINPROJECT } from "src/common/enums/enums";
import { Actor } from "src/shared/acl/actor.constant";
import { RequestContext } from "src/shared/request-context/request-context.dto";
import { EntityRepository, Repository } from "typeorm";

import { UpdateProjectMemberDto } from "../dto/update-project-member.dto";
import { ProjectMember } from "../entities/project-member.entity";



@EntityRepository(ProjectMember)
export class ProjectMemberRepository extends Repository<ProjectMember>{
  
  async getProjectMembersByProjectId(id: number): Promise<ProjectMember[]>{
    const projectMember = await this.find({
      relations: ['user'],
      where: {
        project: {
        id
        },
        userStatus: USERSTATUSINPROJECT.ACTIVE
      }
    })
    return projectMember;
  }

  async getProjectsListForCurrentUsers(ctx: RequestContext): Promise<ProjectMember[]> {
    const actor: Actor = ctx.user
    const currentUserDetails = await this.find({
      relations: ['project'],
      where: {
        user: {
          id: actor.id
        },
        userStatus: USERSTATUSINPROJECT.ACTIVE
      }
    })
    return currentUserDetails;
  }


  async getCurrentCurrentDetailsInProjects(ctx: RequestContext, projectId: number): Promise<ProjectMember>{
    const actor: Actor = ctx.user
    const currentUserDetails = await this.findOne({
      relations: ['user'],
      where: {
        project: {
          id:projectId
        },
        user: {
          id:actor.id
        }
      }
    })
    return currentUserDetails;
  }



  
}
