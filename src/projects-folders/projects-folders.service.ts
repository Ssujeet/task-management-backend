import { Injectable } from '@nestjs/common';

import { CreateProjectsFolderDto } from './dto/create-projects-folder.dto';
import { UpdateProjectsFolderDto } from './dto/update-projects-folder.dto';

@Injectable()
export class ProjectsFoldersService {
  create(createProjectsFolderDto: CreateProjectsFolderDto) {
    return 'This action adds a new projectsFolder';
  }

  findAll() {
    return `This action returns all projectsFolders`;
  }

  findOne(id: number) {
    return `This action returns a #${id} projectsFolder`;
  }

  update(id: number, updateProjectsFolderDto: UpdateProjectsFolderDto) {
    return `This action updates a #${id} projectsFolder`;
  }

  remove(id: number) {
    return `This action removes a #${id} projectsFolder`;
  }
}
