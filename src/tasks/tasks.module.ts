import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectMemberRepository } from 'src/project-member/repositories/project-member.respositories';
import { TaskStatusModule } from 'src/task-status/task-status.module';
import { UserModule } from 'src/user/user.module';

import { TaskRepositories } from './repositories/tasks.repositories';
import { TasksController } from './tasks.controller';
import { TasksService } from './tasks.service';

@Module({
  imports :[TypeOrmModule.forFeature([TaskRepositories,ProjectMemberRepository,]), TaskStatusModule,UserModule],
  controllers: [TasksController],
  providers: [TasksService],
  exports:[TasksService]
})
export class TasksModule {}
