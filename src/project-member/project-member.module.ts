import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectsModule } from 'src/projects/projects.module';
import { UserModule } from 'src/user/user.module';

import { ProjectMemberController } from './project-member.controller';
import { ProjectMemberService } from './project-member.service';
import { ProjectMemberRepository } from './repositories/project-member.respositories';

@Module({
  imports: [TypeOrmModule.forFeature([ProjectMemberRepository]),UserModule, ProjectsModule],
  controllers: [ProjectMemberController],
  providers: [ProjectMemberService],
  exports :[ProjectMemberService]
})
export class ProjectMemberModule {}
