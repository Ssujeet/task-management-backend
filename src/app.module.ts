import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { ProjectFilesModule } from './project-files/project-files.module';
import { ProjectMemberModule } from './project-member/project-member.module';
import { ProjectsModule } from './projects/projects.module';
import { ProjectsFoldersModule } from './projects-folders/projects-folders.module';
import { SharedModule } from './shared/shared.module';
import { TaskStatusModule } from './task-status/task-status.module';
import { TasksModule } from './tasks/tasks.module';
import { UserModule } from './user/user.module';
import { TaskMemberModule } from './task-member/task-member.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.DB_HOST,
      port: process.env.DB_PORT ? parseInt(process.env.DB_PORT, 10) : null,
      username: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DB_NAME,
      entities: ["dist/**/*.entity{.ts,.js}"],
      migrations: [__dirname + '/migrations/**/*{.ts,.js}'],
      migrationsRun: true,
      cli: {
        entitiesDir: 'src',
        migrationsDir: 'migrations',
      },
      // Timezone configured on the MySQL server.
      // This is used to typecast server date/time values to JavaScript Date object and vice versa.
      timezone: 'Z',
      synchronize: true,
      debug: process.env.NODE_ENV === 'development' ? true : false,
    }),
    SharedModule,
    UserModule,
    AuthModule,
    ProjectsModule,
    TasksModule,
    ProjectsFoldersModule,
    ProjectFilesModule,
    ProjectMemberModule,
    TaskStatusModule,
    TaskMemberModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
