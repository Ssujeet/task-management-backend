import { Test, TestingModule } from '@nestjs/testing';
import { TaskMemberService } from './task-member.service';

describe('TaskMemberService', () => {
  let service: TaskMemberService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TaskMemberService],
    }).compile();

    service = module.get<TaskMemberService>(TaskMemberService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
