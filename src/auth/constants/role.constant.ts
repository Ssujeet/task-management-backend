export enum ROLE {
  USER = 'USER',
  ADMIN = 'ADMIN',
  MEMBER = "MEMBER"
}
