import { ApiProperty } from "@nestjs/swagger";
import { Expose } from "class-transformer";
import { IsEnum, IsNotEmpty, IsNumber } from "class-validator";
import { PROJECTMEMBERROLE, USERSTATUSINPROJECT } from "src/common/enums/enums";
import { Project } from "src/projects/entities/project.entity";

export class ProjectListsDto {



  @Expose()
  project: Project;


  @Expose()
  role: PROJECTMEMBERROLE
  
  @Expose()
  userStatus: USERSTATUSINPROJECT

}
