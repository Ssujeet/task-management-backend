import { PartialType } from '@nestjs/swagger';
import { CreateProjectsFolderDto } from './create-projects-folder.dto';

export class UpdateProjectsFolderDto extends PartialType(
  CreateProjectsFolderDto,
) {}
