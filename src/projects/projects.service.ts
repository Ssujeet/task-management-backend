import { Injectable } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { PROJECTMEMBERROLE } from 'src/common/enums/enums';
import { ProjectMember } from 'src/project-member/entities/project-member.entity';
import { Actor } from 'src/shared/acl/actor.constant';
import { AppLogger } from 'src/shared/logger/logger.service';
import { RequestContext } from 'src/shared/request-context/request-context.dto';
import { TaskStatus } from 'src/task-status/entities/task-status.entity';
import { User } from 'src/user/entities/user.entity';
import { UserService } from 'src/user/services/user.service';

import { CreateProjectDto } from './dto/create-project.dto';
import { ProjectOutputDto } from './dto/project-output.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { Project } from './entities/project.entity';
import { ProjectRepository } from './repositories/project.repositories';


const defaultTaskName = ["TODO", "PROGRESS", "DONE"]
@Injectable()
export class ProjectsService {
  constructor(
    private projectRepository: ProjectRepository,
    private userService: UserService,

    private readonly logger: AppLogger,
  ) {
    this.logger.setContext(ProjectsService.name);
  }

  async create(ctx: RequestContext,createProjectDto: CreateProjectDto):Promise<ProjectOutputDto> {
    const project = plainToClass(Project, createProjectDto)
    const actor: Actor = ctx.user
    const user = await this.userService.getUserById(ctx, actor.id);
    const projectMember = new ProjectMember();
    projectMember.user = plainToClass(User, user);
    projectMember.role = PROJECTMEMBERROLE.LEADER;
    project.projectMember = [plainToClass(ProjectMember, projectMember)];
    const taskStatusArray = []
    defaultTaskName.forEach((item) => {
      const taskStatus = new TaskStatus();
      taskStatus.taskName = item
      taskStatusArray.push(taskStatus)
    })

    project.taskStatus = taskStatusArray
    this.projectRepository.save(project)
    return plainToClass(ProjectOutputDto, project,)
  }


  async findOne(id: number): Promise<ProjectOutputDto> {
    const project = await this.projectRepository.getById(id)
    return plainToClass(ProjectOutputDto, project, {excludeExtraneousValues:true})
  }

  

  async update(id: number, updateProjectDto: UpdateProjectDto): Promise<ProjectOutputDto> {
    const project = await this.projectRepository.getById(id)
    const updatedProject: Project = { ...project, ...plainToClass(UpdateProjectDto, updateProjectDto) }
    await this.projectRepository.save(updatedProject)
    return plainToClass(ProjectOutputDto, updateProjectDto,{excludeExtraneousValues:true})
  }
}
