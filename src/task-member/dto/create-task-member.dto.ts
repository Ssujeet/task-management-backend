import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger"
import { IsEnum, IsNotEmpty,IsNumber,IsOptional,IsString } from "class-validator"
import { AbstractDto } from "src/common/dto/abstract.dto"
import { USERSTATUSINTASK } from "src/common/enums/enums"

export class CreateTaskMemberDto extends AbstractDto {
  

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  taskId: number

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  projectId: number

  
  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  userId: number
  

  @ApiPropertyOptional()
  @IsEnum(USERSTATUSINTASK)
  @IsOptional()
  userStatus:USERSTATUSINTASK


}
