import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectMemberRepository } from 'src/project-member/repositories/project-member.respositories';
import { TasksModule } from 'src/tasks/tasks.module';
import { UserModule } from 'src/user/user.module';

import { TaskMemberRespositories } from './repository/task-member.repositories';
import { TaskMemberController } from './task-member.controller';
import { TaskMemberService } from './task-member.service';

@Module({
  imports: [TypeOrmModule.forFeature([TaskMemberRespositories, ProjectMemberRepository,]), TasksModule, UserModule],
  controllers: [TaskMemberController],
  providers: [TaskMemberService]
})
export class TaskMemberModule {}
