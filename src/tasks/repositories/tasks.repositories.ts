import { NotFoundException } from "@nestjs/common";
import { EntityRepository,Repository } from "typeorm";

import { Task } from "../entities/task.entity";



@EntityRepository(Task)
export class TaskRepositories extends Repository<Task>{
  

  async getById(id: number): Promise<Task> {
    const task = await this.findOne(id);
  
    if (!task) {
      throw new NotFoundException();
    }
    return task
  }

}
