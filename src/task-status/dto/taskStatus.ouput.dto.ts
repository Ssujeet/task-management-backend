import { ApiProperty } from "@nestjs/swagger";
import { Expose } from "class-transformer";
import { AbstractDto } from "src/common/dto/abstract.dto";
import { Project } from "src/projects/entities/project.entity";

export class TaskStatusOutputDto extends AbstractDto{

  @Expose()
  taskName: string
  
  @Expose()
  project:Project
}
