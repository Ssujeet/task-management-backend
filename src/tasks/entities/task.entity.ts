import { AbstractEntity } from "src/common/entity/asbstract-entity";
import { TASKSPRIORITY } from "src/common/enums/enums";
import { TaskMember } from "src/task-member/entities/task-member.entity";
import { TaskStatus } from "src/task-status/entities/task-status.entity";
import { User } from "src/user/entities/user.entity";
import { Column,Entity, ManyToOne, OneToMany } from "typeorm";

@Entity()
export class Task extends AbstractEntity {


  @Column({ length: 100, nullable: false })
  title: string;


  @Column({ length: 500, nullable: false })
  descriptions: string;

  @Column({
    type: 'enum',
    enum: TASKSPRIORITY,
    default: [TASKSPRIORITY.MEDUIM],
  })
  priority: TASKSPRIORITY

  @ManyToOne(() => TaskStatus)
  taskStatus: TaskStatus;


  @OneToMany(() => TaskMember, taskMember => taskMember.task, { cascade: true })
  taskMember: TaskMember[];

  createdUser: User;



}
