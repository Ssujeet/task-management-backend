import { Expose } from "class-transformer";
import { AbstractDto } from "src/common/dto/abstract.dto";
import { USERSTATUSINTASK } from "src/common/enums/enums";
import { Task } from "src/tasks/entities/task.entity";
import { User } from "src/user/entities/user.entity";

export class OutputTaskMemberDto extends AbstractDto{

  @Expose()
  task: Task

  @Expose()
  user: User

  @Expose()
  userStatus: USERSTATUSINTASK
}
