import { AbstractEntity } from "src/common/entity/asbstract-entity";
import { USERSTATUSINTASK } from "src/common/enums/enums";
import { Task } from "src/tasks/entities/task.entity";
import { User } from "src/user/entities/user.entity";
import { Column,Entity, ManyToOne } from "typeorm";

@Entity()
export class TaskMember extends AbstractEntity {

  @ManyToOne(() => Task, task=>task.taskMember)
  task: Task;


  @ManyToOne(() => User)
  user: User;


  @Column({
    type: 'enum',
    enum: USERSTATUSINTASK,
    default: USERSTATUSINTASK.ACTIVE
  })
  userStatus: USERSTATUSINTASK

 }
