import { ApiProperty,PartialType } from '@nestjs/swagger';
import { IsEnum,IsNotEmpty, IsNumber } from 'class-validator';
import { PROJECTMEMBERROLE, USERSTATUSINPROJECT } from 'src/common/enums/enums';

import { CreateProjectMemberDto } from './create-project-member.dto';

export class UpdateProjectMemberDto extends CreateProjectMemberDto{

  @IsNotEmpty()
  @ApiProperty()
  @IsNumber()
  id:number

  @IsNotEmpty()
  @ApiProperty()
  @IsEnum(USERSTATUSINPROJECT)
  userStatus:USERSTATUSINPROJECT
}
