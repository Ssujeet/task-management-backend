import { Body, ClassSerializerInterceptor,Controller, Delete,Get, Param, Patch, Post, Put,UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { debug } from 'console';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RESPONSESTATUS } from 'src/common/enums/enums';
import { returnResponse } from 'src/common/utils/utils.functions';
import { BaseApiResponse } from 'src/shared/dtos/base-api-response.dto';
import { ReqContext } from 'src/shared/request-context/req-context.decorator';
import { RequestContext } from 'src/shared/request-context/request-context.dto';

import { CreateTaskStatusDto } from './dto/create-task-status.dto';
import { TaskStatusOutputDto } from './dto/taskStatus.ouput.dto';
import { UpdateTaskStatusDto } from './dto/update-task-status.dto';
import { TaskStatusService } from './task-status.service';

@Controller('task-status')
export class TaskStatusController {
  constructor(private readonly taskStatusService: TaskStatusService) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  async create(@ReqContext() ctx: RequestContext, @Body() createTaskStatusDto: CreateTaskStatusDto): Promise<BaseApiResponse<TaskStatusOutputDto | string>> {
    const taskStatus = await this.taskStatusService.create(ctx, createTaskStatusDto);
    return returnResponse(taskStatus,"Successfull Creation", RESPONSESTATUS.SUCCESS, {})
  }


  @Get('listsByProjectId/:id')
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async getTasksStatusListByProjectId(@Param('id') id: string): Promise<BaseApiResponse<TaskStatusOutputDto[] | string>> {
    const taskStatusLists = await this.taskStatusService.getTaskStatusListsByProjectId(+id);

    return returnResponse(taskStatusLists, "Successfull Creation", RESPONSESTATUS.SUCCESS, {})
  }

  @Put()
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async update(@ReqContext() ctx: RequestContext, @Body() updateTaskStatusDto: UpdateTaskStatusDto): Promise<BaseApiResponse<TaskStatusOutputDto>> {
    const updatedTaskLists = await this.taskStatusService.update(ctx,updateTaskStatusDto)
    return returnResponse(updatedTaskLists, "Successfull Updated", RESPONSESTATUS.SUCCESS, {})
  }

  @Delete(':id/projectId/:projecId')
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async remove(@ReqContext() ctx: RequestContext, @Param('id') id: string, @Param('projecId') projecId: string): Promise<BaseApiResponse<TaskStatusOutputDto>> {
    const deletedTaskStatus = await this.taskStatusService.remove(ctx,+id, +projecId);
    return returnResponse(deletedTaskStatus, "Successfull Deleted", RESPONSESTATUS.SUCCESS, {})
  }
}
