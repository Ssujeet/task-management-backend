import { Module } from '@nestjs/common';

import { ProjectsFoldersController } from './projects-folders.controller';
import { ProjectsFoldersService } from './projects-folders.service';

@Module({
  controllers: [ProjectsFoldersController],
  providers: [ProjectsFoldersService],
})
export class ProjectsFoldersModule {}
