
import { ApiProperty,ApiPropertyOptional,PartialType } from '@nestjs/swagger';
import { IsArray,IsNotEmpty, IsNumber,IsOptional,IsString } from 'class-validator';
import { PROJECTSTATUS } from 'src/common/enums/enums';

import { CreateProjectDto } from './create-project.dto';

export class UpdateProjectDto extends PartialType(CreateProjectDto) {

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  id: number;


  @IsOptional()
  @ApiPropertyOptional()
  projectStatus : PROJECTSTATUS
}
