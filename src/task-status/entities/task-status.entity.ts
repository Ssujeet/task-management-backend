import { AbstractEntity } from "src/common/entity/asbstract-entity";
import { Project } from "src/projects/entities/project.entity";
import { Column,Entity, ManyToOne } from "typeorm";

@Entity()
export class TaskStatus  extends AbstractEntity{

  @Column({ length: 100, nullable: false })
  taskName: string
  
  @ManyToOne(() => Project, project => project.taskStatus)
  project: Project;

}
