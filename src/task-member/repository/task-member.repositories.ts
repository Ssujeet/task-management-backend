import { NotFoundException } from "@nestjs/common";
import { EntityRepository, Repository } from "typeorm";

import { TaskMember } from "../entities/task-member.entity";
@EntityRepository(TaskMember)
export class TaskMemberRespositories extends Repository<TaskMember>{

  async getById(id: number): Promise<TaskMember>{

    const taskMember = await this.findOne(id)
    if (!taskMember) {
      throw new NotFoundException();
    }
    return taskMember
}
}
