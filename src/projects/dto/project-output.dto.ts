import { Expose } from "class-transformer";
import { AbstractDto } from "src/common/dto/abstract.dto";
import { PROJECTSTATUS } from "src/common/enums/enums";
import { ProjectMember } from "src/project-member/entities/project-member.entity";
import { TaskStatus } from "src/task-status/entities/task-status.entity";


export class ProjectOutputDto extends AbstractDto{

  @Expose()
  projectMember: ProjectMember[]

  @Expose()
  projectStatus: PROJECTSTATUS

  @Expose()
  projectName: string;

  @Expose()
  startDate: Date;

  @Expose()
  endDate: Date;

  @Expose()
  descriptions: string;

  taskStatus:TaskStatus[]

}
