import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';

import { CreateProjectFileDto } from './dto/create-project-file.dto';
import { UpdateProjectFileDto } from './dto/update-project-file.dto';
import { ProjectFilesService } from './project-files.service';

@Controller('project-files')
export class ProjectFilesController {
  constructor(private readonly projectFilesService: ProjectFilesService) {}

  @Post()
  create(@Body() createProjectFileDto: CreateProjectFileDto) {
    return this.projectFilesService.create(createProjectFileDto);
  }

  @Get()
  findAll() {
    return this.projectFilesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.projectFilesService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateProjectFileDto: UpdateProjectFileDto,
  ) {
    return this.projectFilesService.update(+id, updateProjectFileDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.projectFilesService.remove(+id);
  }
}
