import { Injectable, UnauthorizedException } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { PROJECTMEMBERROLE, USERSTATUSINTASK } from 'src/common/enums/enums';
import { ProjectMemberService } from 'src/project-member/project-member.service';
import { ProjectMemberRepository } from 'src/project-member/repositories/project-member.respositories';
import { RequestContext } from 'src/shared/request-context/request-context.dto';
import { Task } from 'src/tasks/entities/task.entity';
import { TasksService } from 'src/tasks/tasks.service';
import { User } from 'src/user/entities/user.entity';
import { UserService } from 'src/user/services/user.service';

import { CreateTaskMemberDto } from './dto/create-task-member.dto';
import { OutputTaskMemberDto } from './dto/output-task-member.dto';
import { UpdateTaskMemberDto } from './dto/update-task-member.dto';
import { TaskMember } from './entities/task-member.entity';
import { TaskMemberRespositories } from './repository/task-member.repositories';

@Injectable()
export class TaskMemberService {


  constructor(private taskMemberRepository: TaskMemberRespositories, private taskService: TasksService, private userService: UserService, private projectMemberRespository : ProjectMemberRepository) {
    
  }
  async create(ctx: RequestContext,createTaskMemberDto: CreateTaskMemberDto):Promise<OutputTaskMemberDto> {
    const { taskId, userId, userStatus, projectId } = createTaskMemberDto
    const userDetailsInProject = await this.projectMemberRespository.getCurrentCurrentDetailsInProjects(ctx, projectId);
    if (!userDetailsInProject || userDetailsInProject.role === PROJECTMEMBERROLE.MEMBER) {
      throw new UnauthorizedException()
    }
    const taskDetails = await this.taskService.findOne(taskId)
    console.log(userId, taskDetails )
    const userDetails = await this.userService.getUserById(ctx, userId)
    console.log(userDetails, taskDetails )
    
    const taskMember = new TaskMember()
    taskMember.user = plainToClass(User,userDetails);
    if (userStatus) { 
      taskMember.userStatus = userStatus
    }
    taskMember.task = plainToClass(Task, taskDetails)
    await this.taskMemberRepository.save(taskMember)

    return plainToClass(OutputTaskMemberDto, taskMember, {excludeExtraneousValues:true} )
    
  }

  async removeUserFromTask(taskMemberId: number): Promise<OutputTaskMemberDto>{
    const taskMemberDetail = await this.taskMemberRepository.getById(taskMemberId)
    taskMemberDetail.userStatus = USERSTATUSINTASK.DEACTIVE
    this.taskMemberRepository.save(taskMemberDetail)
    return plainToClass(OutputTaskMemberDto, taskMemberDetail, { excludeExtraneousValues: true })
  }

  findAll() {
    return `This action returns all taskMember`;
  }

  findOne(id: number) {
    return `This action returns a #${id} taskMember`;
  }

  update(id: number, updateTaskMemberDto: UpdateTaskMemberDto) {
    return `This action updates a #${id} taskMember`;
  }

  remove(id: number) {
    return `This action removes a #${id} taskMember`;
  }
}
