import { BaseApiResponse } from "src/shared/dtos/base-api-response.dto"

import { RESPONSESTATUS } from "../enums/enums"

export const returnResponse = (data :any,message:string,status:RESPONSESTATUS,meta:any):BaseApiResponse<any> => {
  return {
    data,
    message,
    status,
    meta,
}
}
