import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty,IsNumber,IsString } from "class-validator";

export class CreateTaskStatusDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  taskName: string
  
  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  projectId: number;
}
