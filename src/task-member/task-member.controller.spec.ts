import { Test, TestingModule } from '@nestjs/testing';

import { TaskMemberController } from './task-member.controller';
import { TaskMemberService } from './task-member.service';

describe('TaskMemberController', () => {
  let controller: TaskMemberController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TaskMemberController],
      providers: [TaskMemberService],
    }).compile();

    controller = module.get<TaskMemberController>(TaskMemberController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
