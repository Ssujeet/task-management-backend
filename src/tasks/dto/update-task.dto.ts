import { ApiProperty,PartialType } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';

import { CreateTaskDto } from './create-task.dto';

export class UpdateTaskDto extends PartialType(CreateTaskDto) {


  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  id:number
}
