import { Expose } from "class-transformer";
import { IsArray, IsNotEmpty } from "class-validator";
import { PROJECTMEMBERROLE,PROJECTSTATUS, USERSTATUSINPROJECT } from "src/common/enums/enums";
import { UserOutput } from "src/user/dtos/user-output.dto";
import { User } from "src/user/entities/user.entity";

export class ProjectMemberbyProjectId {



  @IsNotEmpty()
  @Expose()
  users: UserOutput

  @Expose()
  role:PROJECTMEMBERROLE


  @Expose()
  userStatus: USERSTATUSINPROJECT
  
}
