import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';

import { CreateProjectsFolderDto } from './dto/create-projects-folder.dto';
import { UpdateProjectsFolderDto } from './dto/update-projects-folder.dto';
import { ProjectsFoldersService } from './projects-folders.service';

@Controller('projects-folders')
export class ProjectsFoldersController {
  constructor(
    private readonly projectsFoldersService: ProjectsFoldersService,
  ) {}

  @Post()
  create(@Body() createProjectsFolderDto: CreateProjectsFolderDto) {
    return this.projectsFoldersService.create(createProjectsFolderDto);
  }

  @Get()
  findAll() {
    return this.projectsFoldersService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.projectsFoldersService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateProjectsFolderDto: UpdateProjectsFolderDto,
  ) {
    return this.projectsFoldersService.update(+id, updateProjectsFolderDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.projectsFoldersService.remove(+id);
  }
}
