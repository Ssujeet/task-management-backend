import { NotFoundException } from "@nestjs/common";
import { EntityRepository, Repository } from "typeorm";

import { TaskStatus } from "../entities/task-status.entity";

@EntityRepository(TaskStatus)
export class TaskStatusRepository extends Repository<TaskStatus>{
  async getById(id: number): Promise<TaskStatus> {
    const taskStatus = await this.findOne({ relations: ['project'],where:{id:id}});
    if (!taskStatus) {
      throw new NotFoundException();
    }
    return taskStatus
  }


  async getTaskStatusListByProjectId(projectId: number): Promise<TaskStatus[]>{
    const taskStatusLists = await this.find({ where: { project: { id: projectId } } })
    if (taskStatusLists) { 
      return taskStatusLists
    }
    throw new NotFoundException();
  }
}
