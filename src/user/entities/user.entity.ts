import { AbstractEntity } from 'src/common/entity/asbstract-entity';
import { ProjectMember } from 'src/project-member/entities/project-member.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  Unique,
  UpdateDateColumn,
} from 'typeorm';

import { ROLE } from '../../auth/constants/role.constant';

@Entity('users')
export class User extends AbstractEntity {


  @Column({ length: 100 })
  name: string;

  @Column()
  password: string;

  @Unique('username', ['username'])
  @Column({ length: 200 })
  username: string;

  @Column({
    type: 'enum',
    enum: ROLE,
    default: [ROLE.MEMBER],
  })
  roles: ROLE[];

  @Column()
  isAccountDisabled: boolean;

  @Unique('email', ['email'])
  @Column({ length: 200 })
  email: string;

  @CreateDateColumn({ name: 'createdAt', nullable: true })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updatedAt', nullable: true })
  updatedAt: Date;

  @OneToMany(() => ProjectMember, projectMember => projectMember.user)
  projectMember: ProjectMember[]


}
