import { Injectable, UnauthorizedException } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { PROJECTMEMBERROLE } from 'src/common/enums/enums';
import { ProjectMemberRepository } from 'src/project-member/repositories/project-member.respositories';
import { ProjectsService } from 'src/projects/projects.service';
import { BaseApiResponse } from 'src/shared/dtos/base-api-response.dto';
import { RequestContext } from 'src/shared/request-context/request-context.dto';
import { UserService } from 'src/user/services/user.service';

import { CreateTaskStatusDto } from './dto/create-task-status.dto';
import { TaskStatusOutputDto } from './dto/taskStatus.ouput.dto';
import { UpdateTaskStatusDto } from './dto/update-task-status.dto';
import { TaskStatus } from './entities/task-status.entity';
import { TaskStatusRepository } from './repositories/taskStatus.repositories';

@Injectable()
export class TaskStatusService {
  constructor(
    private projectMemberRespository: ProjectMemberRepository,
    private userService: UserService,
    private projectService: ProjectsService,
    private taskStatusRepository : TaskStatusRepository

  ) {
    // this.logger.setContext(ProjectsService.name);
  }


  async getById(id: number): Promise<TaskStatusOutputDto >{
    
    const taskStatus = await this.taskStatusRepository.getById(+id)
  
    
    return  taskStatus
  }

  async create(ctx: RequestContext, createTaskStatusDto: CreateTaskStatusDto): Promise<TaskStatusOutputDto | string> {


    const { taskName, projectId } = createTaskStatusDto

    
    const userDetailsInProject = await this.projectMemberRespository.getCurrentCurrentDetailsInProjects(ctx, projectId);

    if (!userDetailsInProject || userDetailsInProject.role === PROJECTMEMBERROLE.MEMBER) {
      throw new UnauthorizedException();
    }

    const project = await this.projectService.findOne(projectId)

    const newTaskStatus = new TaskStatus()
    newTaskStatus.project = project
    newTaskStatus.taskName = taskName
    await this.taskStatusRepository.save(newTaskStatus);
    return plainToClass(TaskStatusOutputDto, newTaskStatus,{excludeExtraneousValues:true})

  }


  async getTaskStatusListsByProjectId(projectId: number): Promise<TaskStatusOutputDto[]>{
    const taskStatusLists = await this.taskStatusRepository.getTaskStatusListByProjectId(projectId)
    return plainToClass(TaskStatusOutputDto, taskStatusLists, {excludeExtraneousValues:true})
  }



  async findOne(id: number): Promise<TaskStatusOutputDto> {
    const taskStatusDetails = await this.taskStatusRepository.getById(id)
    return plainToClass(TaskStatusOutputDto, taskStatusDetails, { excludeExtraneousValues: true })
  }

  async update(ctx: RequestContext, updateTaskStatusDto: UpdateTaskStatusDto): Promise<TaskStatusOutputDto> {

    const {projectId} = updateTaskStatusDto
    const userDetailsInProject = await this.projectMemberRespository.getCurrentCurrentDetailsInProjects(ctx, projectId);
    if (!userDetailsInProject || userDetailsInProject.role === PROJECTMEMBERROLE.MEMBER) {
      throw new UnauthorizedException()
    }
    const updatedTaskStatus: TaskStatus = { ...updateTaskStatusDto, ...plainToClass(TaskStatus, updateTaskStatusDto) }
    await this.taskStatusRepository.save(updatedTaskStatus)
    return plainToClass(TaskStatusOutputDto, updatedTaskStatus, {
    })
  }



  async remove(ctx: RequestContext, id: number, projectId:number): Promise<TaskStatusOutputDto> {
   
    const userDetailsInProject = await this.projectMemberRespository.getCurrentCurrentDetailsInProjects(ctx, projectId);
    if (!userDetailsInProject || userDetailsInProject.role === PROJECTMEMBERROLE.MEMBER) {
      throw new UnauthorizedException()
    }
    const removeTaskStatus = await this.taskStatusRepository.delete(id)
    return plainToClass(TaskStatusOutputDto, removeTaskStatus, {excludeExtraneousValues:true} )
  }
}
