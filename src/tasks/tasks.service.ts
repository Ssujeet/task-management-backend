import { Injectable, UnauthorizedException } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { PROJECTMEMBERROLE, USERSTATUSINTASK } from 'src/common/enums/enums';
import { ProjectMemberRepository } from 'src/project-member/repositories/project-member.respositories';
import { Actor } from 'src/shared/acl/actor.constant';
import { RequestContext } from 'src/shared/request-context/request-context.dto';
import { TaskMember } from 'src/task-member/entities/task-member.entity';
import { TaskStatus } from 'src/task-status/entities/task-status.entity';
import { TaskStatusService } from 'src/task-status/task-status.service';
import { User } from 'src/user/entities/user.entity';
import { UserService } from 'src/user/services/user.service';

import { CreateTaskDto } from './dto/create-task.dto';
import { OutputTaskDto } from './dto/output-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { Task } from './entities/task.entity';
import { TaskRepositories } from './repositories/tasks.repositories';

@Injectable()
export class TasksService {


  constructor(private projectMemberRespository: ProjectMemberRepository,private taskRepository : TaskRepositories, private taskStatusService : TaskStatusService, private userService: UserService) {
    


  }
  async create(ctx: RequestContext, createTaskDto: CreateTaskDto):Promise<OutputTaskDto> {
    const actor: Actor = ctx.user
    const createdUser = await this.userService.getUserById(ctx, actor.id);
    const { taskStatusId, taskMemberId } = createTaskDto
    const taskMemberLists = []
    const taskDetail = await this.taskStatusService.getById(taskStatusId)
    taskMemberId.forEach((item) => {
      const user = this.userService.getUserById(ctx, item)
      const taskMember = new TaskMember();
      taskMember.user = plainToClass(User, user);
      taskMemberLists.push(taskMember)
    
    })
    const userDetailsInProject = await this.projectMemberRespository.getCurrentCurrentDetailsInProjects(ctx, taskDetail.project.id);

    if (!userDetailsInProject || userDetailsInProject.role === PROJECTMEMBERROLE.MEMBER) {
      throw new UnauthorizedException()
    }
    const task = plainToClass(Task, createTaskDto)

    task.createdUser = plainToClass(User, createdUser);

    task.taskStatus = taskDetail;
    this.taskRepository.save(task);

    if (taskMemberLists.length !== 0) {
      task.taskMember = taskMemberLists
    }

    return plainToClass(OutputTaskDto, task,{excludeExtraneousValues:true});
  }

 

  async findOne(id: number): Promise<OutputTaskDto>{
    const task = await this.taskRepository.getById(id)

    return plainToClass(Task, task)
  }

  async update(ctx: RequestContext, updateTaskDto: UpdateTaskDto): Promise<OutputTaskDto> {
    const {id, taskStatusId} = updateTaskDto
    const task = await this.taskRepository.getById(id)
    const taskStatusDetail = await this.taskStatusService.getById(taskStatusId)
    const userDetailsInProject = await this.projectMemberRespository.getCurrentCurrentDetailsInProjects(ctx, taskStatusDetail.project.id);

    if (!userDetailsInProject || userDetailsInProject.role === PROJECTMEMBERROLE.MEMBER) {
      throw new UnauthorizedException()
    }
    const updateTask: Task = { ...task, ...plainToClass(UpdateTaskDto, updateTaskDto), ...plainToClass(TaskStatus, taskStatusDetail ) }
    await this.taskRepository.save(updateTask)
    return plainToClass(OutputTaskDto, updateTask, { excludeExtraneousValues: true })
  }


  async findTasksListsByProjectId(ctx: RequestContext, id: number): Promise<OutputTaskDto[]> {
    const userDetailsInProject = await this.projectMemberRespository.getCurrentCurrentDetailsInProjects(ctx, id);
    if (!userDetailsInProject || userDetailsInProject.role === PROJECTMEMBERROLE.MEMBER) {
      throw new UnauthorizedException()
    }
    const tasksLists = await this.taskRepository.find({
      relations: ['taskMember','taskStatus'],
      where: {
        taskStatus: {
          project: {
            id
          }
        }
      }
    })
    
    return plainToClass(OutputTaskDto, tasksLists)
  }

  remove(id: number) {
    return `This action removes a #${id} task`;
  }
}
