import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RESPONSESTATUS } from 'src/common/enums/enums';
import { returnResponse } from 'src/common/utils/utils.functions';
import { BaseApiResponse } from 'src/shared/dtos/base-api-response.dto';
import { ReqContext } from 'src/shared/request-context/req-context.decorator';
import { RequestContext } from 'src/shared/request-context/request-context.dto';

import { CreateTaskDto } from './dto/create-task.dto';
import { OutputTaskDto } from './dto/output-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { TasksService } from './tasks.service';

@Controller('tasks')
export class TasksController {
  constructor(private readonly tasksService: TasksService) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  async create(@ReqContext() ctx: RequestContext, @Body() createTaskDto: CreateTaskDto): Promise<BaseApiResponse<OutputTaskDto>> {
    const task = await this.tasksService.create(ctx, createTaskDto);
    return returnResponse(task,"Successfully Added", RESPONSESTATUS.SUCCESS, {})
  }



  @Get(':id')
  async findOne(@Param('id') id: string): Promise<BaseApiResponse<OutputTaskDto>> {
    const task = await this.tasksService.findOne(+id);
    return returnResponse(task, "Retrieved Successful", RESPONSESTATUS.SUCCESS,{})
  }


  @Put()
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  async update(@ReqContext() ctx: RequestContext, @Body() updateTaskDto: UpdateTaskDto): Promise<BaseApiResponse<OutputTaskDto>> {
    const tasks = await this.tasksService.update(ctx, updateTaskDto);
    return returnResponse(tasks, "Succesfully Updated", RESPONSESTATUS.SUCCESS,{})
  }


  @Get('getProjectLists/project/:id')
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  async getTaskListsByProjectId(@ReqContext() ctx: RequestContext,@Param('id') id: string): Promise<BaseApiResponse<OutputTaskDto[]>> {
    const tasksLists = await this.tasksService.findTasksListsByProjectId(ctx,+id);
    return returnResponse(tasksLists, "Succesfully Updated", RESPONSESTATUS.SUCCESS, {})
  }

  

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.tasksService.remove(+id);
  }
}
