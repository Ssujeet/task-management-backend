import { AbstractEntity } from "src/common/entity/asbstract-entity";
import { PROJECTSTATUS } from "src/common/enums/enums";
import { ProjectMember } from "src/project-member/entities/project-member.entity";
import { TaskStatus } from "src/task-status/entities/task-status.entity";
import { User } from "src/user/entities/user.entity";
import { Column,Entity, JoinTable,ManyToMany, OneToMany } from "typeorm";

@Entity("project")
export class Project extends AbstractEntity {

  @Column({ length: 100, nullable: false})
  projectName: string;

  @Column({nullable:false})
  startDate: Date


  @Column({ nullable: false })
  endDate: Date
  
  @Column({length:300, nullable:true})
  descriptions: string

  
  @OneToMany(() => ProjectMember, projectMember => projectMember.project, {
  cascade:true, onDelete:"CASCADE"
  })
  projectMember: ProjectMember[]

  @OneToMany(() => TaskStatus, TaskStatus => TaskStatus.project, {
    cascade: true, onDelete: "CASCADE"
  })
  taskStatus: TaskStatus[]
  
  @Column({
    type: 'enum',
    enum: PROJECTSTATUS,
    default: PROJECTSTATUS.ONPROGRESS,
  })
  projectStatus: PROJECTSTATUS

 }
